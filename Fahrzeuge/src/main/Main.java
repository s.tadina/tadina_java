package main;

import java.util.Iterator;
import java.util.Scanner;

import vehicles.LKW;
import vehicles.PKW;
import carshop.Carshop;

public class Main {
    public static void main(String[] args) {

        Carshop carshop = new Carshop(10, "Huber");
        carshop.setFahrzeugList("PKW", 1, "Audi", 2008, 2000, 2021);
        carshop.setFahrzeugList("LKW", 2, "VW", 2018, 2500, -1);

        System.out.println("Im Autohaus sind derzeit " + carshop.numberVehicles() + " Fahrzeuge.");

        System.out.println("Überfordert: " + carshop.isOverCapacity());
        System.out.println("Überfordert: " + carshop.isOverCapacity(10));

        System.out.printf("Folgende Marken sind zu verkaufen: ");
        Iterator<String> it = carshop.getBrands().iterator();
        while (it.hasNext()) {
            System.out.print(it.next());
            if (it.hasNext()) {
                System.out.print(", ");
            } else {
                System.out.println("\n");
            }
        }

        carshop.getPrices();
        carshop.pkwLkwCompare();
    }
}

        /*

        Carshop carshop = new Carshop(5, 10, "Huber");

        /*
        PKW pkw1 = new PKW(4, "Audi", 2021, 5000, 2022);
        LKW lkw1 = new LKW(1, "Volvo", 1981, 2000);
        pkw1.setId(7);
        lkw1.setBrand("Audi");
        pkw1.print();
        lkw1.print();

        Scanner scanner = new Scanner(System.in);
        int id = Integer.parseInt(scanner.nextLine());
        String brand = scanner.nextLine();
        int constructYear = Integer.parseInt(scanner.nextLine());
        double price = Double.parseDouble(scanner.nextLine());
        int demo = Integer.parseInt(scanner.nextLine());

        PKW pkw1 = new PKW(id, brand, constructYear, price, demo);
        PKW pkw2 = new PKW(id, brand, constructYear, price, demo);
        PKW pkw3 = new PKW(id, brand, constructYear, price, demo);

        LKW lkw1 = new LKW(id, brand, constructYear, price);
        LKW lkw2 = new LKW(id, brand, constructYear, price);

        PKW[] carBrands = {pkw1, pkw2, pkw3};
        LKW[] truckBrands= {lkw1, lkw2};

        for(int i=0; i<truckBrands.length; i++){
            for (int j=0; j< carBrands.length; j++){
                if (truckBrands[i].getBrand().equals(carBrands[j].getBrand())){
                    System.out.println("Im Autohaus " + carshop.getName() + " gibt es einen PKW und einen LKW, die vom " +
                            "gleichen Hersteller sind" );
                    break;
                }else{
                    System.out.println("Es gibt keine gleichen Marken von PKW und LKW");
                }
            }
        }

        lkw1.print();
        lkw2.print();
        pkw1.print();
        pkw2.print();
        pkw3.print();

    }
}
    /*
        int numberPkw = 3;
        int numberLkw = 2;
        int numberVehicles = numberPkw + numberLkw;

        for (int i = 1; i <= numberVehicles; i++) {
            System.out.println("Es gibt ein neues Fahrzeug im Autohaus. Anzahl Fahrzeuge: " + i);
            if (i < numberVehicles) {
                System.out.println("Es sind noch nicht alle Fahrzeuge registriert.");
            } else if (i == numberVehicles) {
                System.out.println("Folgende Marken sind zu verkaufen: " + pkw1 + ", " + pkw2 + ", " + pkw3 + ", " + lkw1 + ".");
            }
        }

        if (numberPkw == numberLkw) {
            System.out.println("Es gibt gleich viele LKWs wie PKWs.");
        } else {
            System.out.println("Es gibt nicht gleich viele LKWs wie PKWs.");
        }

        numberVehicles = numberVehicles * 2;
        System.out.println("Es gibt neue Fahrzeuge im Autohaus. Anzahl Fahrzeuge: " + numberVehicles);

        if (discount != 0) {
            System.out.println("Rabatt: " + discount + "%");
        }

        switch (pkw1) {
            case "Audi":
                discount = 5;
                discount *= 5;
                discount /= 2;
                discount = (discount > 15) ? 15 : discount;
                System.out.println("Der Preis des ersten PKWs ist: " + (price - (price * discount / 100)));
                break;
            case "Mercedes":
                discount = 5 + 10;
                discount *= 5;
                discount /= 2;
                discount = (discount > 15) ? 15 : discount;
                System.out.println("Der Preis des ersten PKWs ist: " + (price - (price * discount / 100)));
                break;
            case "VW", "Volvo":
                price = (price % 2 == 0) ? 1500 : 1700;
                System.out.println("Der Preis des ersten PKWs ist: " + price);
                break;
            default:
                System.out.println("Der Preis konnte nicht berechnet werden, da die Marke des Fahrzeugs nicht erkennbar ist.");
        }
*/
