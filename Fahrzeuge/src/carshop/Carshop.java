package carshop;

import vehicles.*;

import java.util.ArrayList;
import java.util.HashSet;

public class Carshop {
    ArrayList<Vehicles> vehiclesList;
    protected int space;
    protected String name;

    public Carshop(int space, String name) {
        this.vehiclesList = new ArrayList<Vehicles>();
        this.space = space;
        this.name = name;
    }

    public boolean isOverCapacity(int staff) {
        if (vehiclesList.size() / (double) staff > 3) {
            return true;
        }
        return false;
    }

    public boolean isOverCapacity() {
        if (vehiclesList.size() > space) {
            return true;
        }
        return false;
    }

    public void print() {
        System.out.println("~~~ " + name + " ~~~");
    }

    public void setFahrzeugList(String fahrzeugTyp, int id, String brand, int constructYear, int price, int demo) {
        if (fahrzeugTyp.toLowerCase().equals("pkw")) {
            vehiclesList.add(new PKW(id, brand, constructYear, price, demo));
        } else if (fahrzeugTyp.toLowerCase().equals("lkw")) {
            vehiclesList.add(new LKW(id, brand, constructYear, price));
        } else {
            System.out.println("Bitte nur PKW oder LKW eingeben.");
        }
    }

    public int numberVehicles() {
        return vehiclesList.size();
    }

    public HashSet<String> getBrands() {
        HashSet<String> brands = new HashSet<>();
        for (Vehicles vehicle : vehiclesList) {
            brands.add(vehicle.getBrand());
        }
        return brands;
    }

    public void getPrices() {
        for (Vehicles vehicle : vehiclesList) {
            String className = vehicle.getClass().toGenericString();
            String[] classNamearr = className.split("\\.");

            System.out.print("ID: " + vehicle.getId() + "\t" + "Marke: " + vehicle.getBrand() + "\t" + "Baujahr: " +
                    vehicle.getConstructYear() + "\t" + "Preis: " + vehicle.getPrice() + "€" + "\t" + "Rabatt: " + vehicle.getRabatt() * 100 + "%" + "\t" +
                    "Endpreis: " + vehicle.calcPrice(vehicle.getRabatt()) + "€" + "\t" + "Das Fahrzeug ist ein " + classNamearr[1] + "\n");
        }
    }

    public void pkwLkwCompare() {
        HashSet<String> pkw = new HashSet<>();
        HashSet<String> lkw = new HashSet<>();
        for (Vehicles vehicle : vehiclesList) {
            if (vehicle.getClass().toString().toLowerCase().contains("pkw")) {
                pkw.add(vehicle.getBrand());
            } else {
                lkw.add(vehicle.getBrand());
            }

        }
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
