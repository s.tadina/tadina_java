package vehicles;

public abstract class Vehicles {
    protected int id;
    protected String brand;
    protected int constructYear;
    protected double price;
    public Vehicles(int id, String brand, int constructYear, double price) {
        this.id = id;
        this.brand = brand;
        this.constructYear = constructYear;
        this.price = price;
    }
    public abstract double getRabatt();
    public abstract void print();
    public double calcPrice(double discount) {
        return price - (price * discount);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getConstructYear() {
        return constructYear;
    }

    public void setConstructYear(int constructYear) {
        this.constructYear = constructYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
