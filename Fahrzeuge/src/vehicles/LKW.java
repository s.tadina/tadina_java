package vehicles;

import java.util.Calendar;

public class LKW extends Vehicles {
    public LKW(int id, String brand, int constructYear, double price) {
        super(id, brand, constructYear, price);
    }

    @Override
    public double getRabatt() {
        int actualYear = Calendar.getInstance().get(Calendar.YEAR);
        int age = actualYear - constructYear;
        double discount = age * 6;
        if (discount > 15) {
            return 0.15;
        } else {
            return discount / 100;
        }
    }

    @Override
    public void print() {
        double discount = getRabatt();
        System.out.print("ID: " + id + "\n" + "Marke: " + brand + "\n" + "Baujahr: " +
                constructYear + "\n" + "Preis: " + price + "\n" + "Rabatt: " + discount + "\n" + "Endpreis: " + calcPrice(discount));
    }
}